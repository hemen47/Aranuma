export const readableDate = (dateString) => {
  const date = new Date(dateString);
  return date.toLocaleDateString().split("/").reverse().join("/");
};
