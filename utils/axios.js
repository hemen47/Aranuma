import axios from "axios";

// initialize axios
export const ax = axios.create({
  withCredentials: true,
});

// jwt token axios interceptor
ax.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("accessToken");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (error) => Promise.reject(error)
);

// general error handling and token refresh interceptor
ax.interceptors.response.use(
  (response) => response,
  async (error) => {
    const originalRequest = error.config;

    // of token has expired, refresh it
    if (error.response.status === 401 && !originalRequest._retry) {
      originalRequest._retry = true;
      try {
        const refreshTokenId = localStorage.getItem("refreshTokenId");
        const previousToken = localStorage.getItem("accessToken");

        const response = await axios.post(
          "api/Auth/RefreshToken",
          {
            refreshTokenId,
          },
          { headers: { Authorization: "Bearer " + previousToken } }
        );
        const { accessToken } = response.data;
        localStorage.setItem("accessToken", accessToken);
        // Retry the original request with the new token
        originalRequest.headers.Authorization = `Bearer ${accessToken}`;
        return axios(originalRequest);
      } catch (error) {
        // refresh token is expired, redirect to login page
        localStorage.removeItem("accessToken");
        localStorage.removeItem("refreshTokenId");
        window.location.assign("/login");
      }
    }
  }
);
