import { useContext, useEffect, useState } from "react";
import { ax } from "../utils/axios";
import { AlertContext } from "./_app";
import { connect } from "mqtt";
import CircularProgress from "@mui/material/CircularProgress";
import UserInfo from "../components/UserInfo";
import Status from "../components/Status";
import Temperature from "../components/Temperature";
import Heartbeat from "../components/Heartbeat";
import BloodPressure from "../components/BloodPressure";
import Oxygen from "../components/Oxygen";
import Map from "../components/Map";
import Wifi from "../components/Wifi";
import Battery from "../components/Battery";
import { useRouter } from "next/router";

export default function Home() {
  const { setError } = useContext(AlertContext);
  const router = useRouter();

  const userInfoInitialState = {
    name: "",
    lastName: "",
    date: "",
    nationalCode: "",
    personelCode: "",
    role: "",
    mobileNum: "",
    watchId: "",
  };

  const healthInfoInitialState = {
    Status: "",
    StatusColor: "",
    BloodOxygen: "",
    BloodOxygenColor: "",
    Temperature: "",
    TemperatureColor: "",
    Heartbeat: "",
    HeartbeatColor: "",
    SYSDIA: "",
    SYSDIAColor: "",
    BatteryLevel: "",
    SignalLevel: "",
    LatLong: [],
  };

  const [healthInfo, setHealthInfo] = useState(healthInfoInitialState);
  const [showSpinner, setShowSpinner] = useState(false);
  const [showCardSkeleton, setShowCardSkeleton] = useState(true);
  const [showSkeleton, setShowSkeleton] = useState(true);
  const [userInfo, setUserInfo] = useState(userInfoInitialState);
  const [client, setClient] = useState(null);
  const [connectStatus, setConnectStatus] = useState("Connect");

  const mqttConnect = () => {
    setConnectStatus("Connecting");
    setClient(
      connect(process.env.NEXT_PUBLIC_BROKER, {
        port: process.env.NEXT_PUBLIC_PORT,
        username: process.env.NEXT_PUBLIC_USERNAME,
        password: process.env.NEXT_PUBLIC_PASSWORD,
        clientId: process.env.NEXT_PUBLIC_CLIENTID,
      })
    );
  };

  const fetchData = async () => {
    try {
      const response = await ax.get("/api/Home/PersonelInfo");
      setUserInfo(response.data);
      setShowSkeleton(false);
    } catch (error) {
      setError(error.response?.data);
    }
  };

  // prevent rendering if not logged in
  useEffect(() => {
    if (!localStorage.getItem("accessToken")) {
      router.push("/login");
    } else {
      setShowSpinner(true);
    }
  }, []);

  // getting user info and connecting to broker
  useEffect(() => {
    mqttConnect();
    fetchData();
  }, []);

  // logging mqtt connection status
  useEffect(() => {
    console.log("Connection Status:", connectStatus);
  }, [connectStatus]);

  // client subscription
  useEffect(() => {
    if (client) {
      client.on("connect", () => {
        setConnectStatus("Connected");
        client.subscribe(process.env.NEXT_PUBLIC_TOPIC, { qos: 0 }, (error) => {
          if (error) {
            return console.log("Subscribe to topics error", error);
          }
        });
      });
      client.on("error", (err) => {
        console.error("Connection error: ", err);
        client.end();
      });
      client.on("reconnect", () => {
        setConnectStatus("Reconnecting");
      });
      client.on("message", (topic, message) => {
        const payload = message.toString();
        setHealthInfo(JSON.parse(payload));
        setShowCardSkeleton(false);
      });
    }
  }, [client]);

  return (
    <main className="mx-auto md:w-[85%] h-[100vh]">
      {!showSpinner ? (
        <CircularProgress className="center" />
      ) : (
        <>
          <div className="flex flex-wrap justify-between">
            <Status
              showCardSkeleton={showCardSkeleton}
              statusColor={healthInfo?.StatusColor}
              status={healthInfo?.Status}
            />
            <Temperature
              showCardSkeleton={showCardSkeleton}
              temperature={healthInfo?.Temperature}
              temperatureColor={healthInfo?.TemperatureColor}
            />
            <Heartbeat
              showCardSkeleton={showCardSkeleton}
              heartbeat={healthInfo?.Heartbeat}
              heartbeatColor={healthInfo?.HeartbeatColor}
            />
            <BloodPressure
              showCardSkeleton={showCardSkeleton}
              bloodPressure={healthInfo?.SYSDIA}
              bloodPressureColor={healthInfo?.SYSDIAColor}
            />
            <Oxygen
              showCardSkeleton={showCardSkeleton}
              bloodOxygen={healthInfo?.BloodOxygen}
              bloodOxygenColor={healthInfo?.BloodOxygenColor}
            />
            <hr style={{ border: "1px solid #c2c2c2" }} />
            <Wifi
              showCardSkeleton={showCardSkeleton}
              signalLevel={healthInfo?.SignalLevel}
            />
            <Battery
              showCardSkeleton={showCardSkeleton}
              batteryLevel={healthInfo?.BatteryLevel}
            />
          </div>
          <div className="flex flex-wrap">
            <UserInfo showSkeleton={showSkeleton} data={userInfo} />
            <Map lat={healthInfo?.LatLong[0]} lng={healthInfo?.LatLong[1]} showCardSkeleton={showCardSkeleton} />
          </div>
        </>
      )}
    </main>
  );
}
