import "./globals.scss";
import { createContext, useEffect, useState } from "react";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import Head from "next/head";

export const AlertContext = createContext();

function MyApp({ Component, pageProps }) {
  const [error, setError] = useState("");
  const [message, setMessage] = useState(null);

  const handleCloseAlert = () => {
    setError("");
    setMessage("");
  };

  return (
    <>
      <AlertContext.Provider value={{ setMessage, setError }}>
        <Head>
          <title>Aranoma</title>
          <meta name="description" content="Welcome to Aranoma" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {/*error toaster message*/}
        <Snackbar
          open={!!error}
          autoHideDuration={6000}
          onClose={handleCloseAlert}
        >
          <Alert onClose={handleCloseAlert} variant="filled" severity={"error"}>
            {error}
          </Alert>
        </Snackbar>
        {/*success and info toaster message*/}
        <Snackbar
          open={!!message}
          autoHideDuration={6000}
          onClose={handleCloseAlert}
        >
          <Alert
            onClose={handleCloseAlert}
            variant="filled"
            severity={"success"}
          >
            {message}
          </Alert>
        </Snackbar>
        <Component {...pageProps} />
      </AlertContext.Provider>
    </>
  );
}

export default MyApp;
