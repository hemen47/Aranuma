import * as React from "react";
import { useContext, useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { IconButton, InputAdornment, InputLabel } from "@mui/material";
import { AlertContext } from "../_app";
import { useRouter } from "next/router";
import Image from "next/image";
import logo from "../../public/logo.svg";
import { Visibility, VisibilityOff } from "@mui/icons-material";
import { ax } from "../../utils/axios";

export default function Login() {
  const router = useRouter();
  const { setError, setMessage } = useContext(AlertContext);

  // show password logic
  const [showPassword, setShowPassword] = useState(false);
  const handleClickShowPassword = () => setShowPassword(!showPassword);
  const handleMouseDownPassword = () => setShowPassword(!showPassword);

  const [usernameError, setUsernameError] = useState(false);
  const [passwordError, serPasswordError] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = new FormData(e.currentTarget);
    const credentials = {
      username: data.get("username"),
      password: data.get("password"),
    };
    if (!credentials.username) {
      setUsernameError(true);
      return;
    } else {
      setUsernameError(false);
    }
    if (!credentials.password) {
      serPasswordError(true);
      return;
    } else {
      serPasswordError(false);
    }

    try {
      const response = await ax.post("/api/Auth/Login", credentials);
      const { accessToken, refreshTokenId, accessTokenExpireDate } =
        response.data;
      setMessage("ورود موفقیت آمیز بود");
      router.push("/");
      localStorage.setItem("accessToken", accessToken);
      localStorage.setItem("refreshTokenId", refreshTokenId);
      localStorage.setItem("accessTokenExpireDate", accessTokenExpireDate);
    } catch (error) {
      setError(error.response?.data);
    }
  };

  return (
    <div>
      <div className="center flex w-96 flex flex-col items-center card p-4">
        <Image quality={100} src={logo} width={120} height={120} alt="logo" />
        <form onSubmit={handleSubmit} className="mt-1">
          <div className="flex items-center">
            <InputLabel
              sx={{ width: "8rem", fontSize: ".8rem" }}
              htmlFor="username"
            >
              نام کاربری
            </InputLabel>
            <TextField
              id="username"
              margin="normal"
              error={usernameError}
              helperText={usernameError ? "لطفا نام کاربری را وارد کنید" : ""}
              fullWidth
              name="username"
              size="small"
              autoComplete="username"
              autoFocus
              inputProps={{
                style: {
                  backgroundColor: "#f5f5f5",
                },
              }}
            />
          </div>
          <div className="flex items-center">
            <InputLabel
              sx={{ width: "8rem", fontSize: ".8rem" }}
              htmlFor="password"
            >
              گذرواژه
            </InputLabel>

            <TextField
              margin="normal"
              fullWidth
              error={passwordError}
              helperText={passwordError ? "لطفا گذرواژه را وارد کنید" : ""}
              size="small"
              type={showPassword ? "text" : "password"}
              id="password"
              name="password"
              autoComplete="current-password"
              InputProps={{
                style: {
                  backgroundColor: "#f5f5f5",
                },
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowPassword}
                      onMouseDown={handleMouseDownPassword}
                    >
                      {showPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
            />
          </div>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            ورود به سامانه
          </Button>
        </form>
      </div>
    </div>
  );
}
