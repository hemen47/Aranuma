import * as React from "react";
import Image from "next/image";
import icon0 from "../public/wifi-0.png";
import icon1 from "../public/wifi-1.png";
import icon2 from "../public/wifi-2.png";
import icon3 from "../public/wifi-3.png";
import icon4 from "../public/wifi-4.png";
import SmallCard from "./SmallCard";

export default function Index({ signalLevel, showCardSkeleton = true }) {
  const renderIcon = () => {
    switch (signalLevel) {
      case "0":
        return icon0;
      case "1":
        return icon1;
      case "2":
        return icon2;
      case "3":
        return icon3;
      default:
        return icon4;
    }
  };

  return (
    <SmallCard title="قدرت فرکانس" showCardSkeleton={showCardSkeleton}>
      <Image
        quality={100}
        className="mt-2"
        src={renderIcon()}
        width={74}
        height={54}
        alt="Signal Level"
      />
      <p style={{ color: "#252525" }} className="font-[900] accent-green-800">
        {signalLevel}
      </p>
    </SmallCard>
  );
}
