import * as React from "react";
import DisabledInput from "./DisabledInput";
import { readableDate } from "../utils/readableDate";
import { Skeleton } from "@mui/material";

export default function UserInfo({ data, showSkeleton = true }) {
  return (
    <>
      {showSkeleton ? (
        <Skeleton animation="wave" className="m-2 min-w-[300px] min-h-[30rem] md:min-h-[40rem] " variant="rectangular"  />
      ) : (
        <div className="m-2 flex justify-center  grow-[1]">
          <div className="w-full min-w-[300px] flex flex-col card">
            <h1 className="text-lg m-6">مشخصات کاربر</h1>
            <DisabledInput value={data?.name} title={"نام"} />
            <DisabledInput value={data?.lastName} title={"نام خانوادگی"} />
            <DisabledInput
              value={readableDate(data?.date)}
              title={"تاریخ تولد"}
            />
            <DisabledInput value={data?.nationalCode} title={"کد ملی"} />
            <DisabledInput value={data?.personelCode} title={"کد پرسنلی"} />
            <DisabledInput value={data?.role} title={"سمت"} />
            <DisabledInput value={data?.mobileNum} title={"شماره موبایل"} />
            <DisabledInput value={data?.watchId} title={"آی دی ساعت"} />
          </div>
        </div>
      )}
    </>
  );
}
