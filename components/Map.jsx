import * as React from "react";
import GoogleMapReact from "google-map-react";
import Brightness1RoundedIcon from "@mui/icons-material/Brightness1Rounded";
import { Skeleton } from "@mui/material";

export default function Map({ lat, lng, showCardSkeleton = true }) {
  const defaultProps = {
    center: {
      lat: 35.73578,
      lng: 51.433062,
    },
    zoom: 16,
  };

  return (
    <>
      {showCardSkeleton ? (
        <Skeleton animation="wave" className="m-2 grow-[8] min-w-[300px] min-h-[30rem] md:min-h-[40rem]" variant="rectangular" />
      ) : (
        <div className="m-2 flex justify-center grow-[8]">
          <div className="w-full min-w-[300px] min-h-[30rem] md:min-h-[40rem] flex flex-col card pt-2">
            <h1 className="text-lg mr-4">نقشه</h1>
            <div style={{ height: "100%", width: "100%" }}>
              <GoogleMapReact
                bootstrapURLKeys={{ key: process.env.NEXT_PUBLIC_MAPKEY }}
                defaultCenter={defaultProps.center}
                defaultZoom={defaultProps.zoom}
              >
                <Brightness1RoundedIcon
                  sx={{ color: "#fc2a2a" }}
                  lat={lat}
                  lng={lng}
                />
              </GoogleMapReact>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
