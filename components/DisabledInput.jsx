import * as React from "react";
import TextField from "@mui/material/TextField";
import { InputLabel } from "@mui/material";

export default function DisabledInput({ title, value }) {
  return (
    <div className="flex items-center md:mx-8 sm:mx-4">
      <InputLabel
        sx={{
          width: "8rem",
          fontSize: ".9rem",
          color: "black",
          fontWeight: "800",
        }}
      >
        {title}
      </InputLabel>

      <TextField
        value={value}
        margin="normal"
        fullWidth
        disabled
        size="small"
        inputProps={{
          style: {
            backgroundColor: "#f5f5f5",
          },
        }}
      />
    </div>
  );
}
