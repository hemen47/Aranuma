import * as React from "react";
import Image from "next/image";
import iconGreen from "../public/bp-green.png";
import iconOrange from "../public/bp-orange.png";
import iconRed from "../public/bp-red.png";
import SmallCard from "./SmallCard";

export default function Index({
  bloodPressure,
  bloodPressureColor = "Green",
  showCardSkeleton = true,
}) {
  const renderIcon = () => {
    switch (bloodPressureColor) {
      case "Green":
        return iconGreen;
      case "Red":
        return iconRed;
      case "Orange":
        return iconOrange;
    }
  };
  return (
    <SmallCard title="فشار خون" showCardSkeleton={showCardSkeleton}>
      <Image
        quality={100}
        src={renderIcon()}
        width={81}
        height={70}
        alt="Blood Pressure"
      />
      <p
        style={{ color: bloodPressureColor }}
        className="font-[.9rem] accent-green-800"
      >
        {bloodPressure}
      </p>
    </SmallCard>
  );
}
