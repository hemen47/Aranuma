import * as React from "react";
import Image from "next/image";
import iconGreen from "../public/heart-green.png";
import iconOrange from "../public/heart-orange.png";
import iconRed from "../public/heart-red.png";
import SmallCard from "./SmallCard";

export default function Index({ heartbeat, heartbeatColor = "Green", showCardSkeleton = true }) {
  const renderIcon = () => {
    switch (heartbeatColor) {
      case "Green":
        return iconGreen;
      case "Red":
        return iconRed;
      case "Orange":
        return iconOrange;
    }
  };
  return (
    <SmallCard title="ضربان قلب" showCardSkeleton={showCardSkeleton}>
      <Image
          quality={100}

          src={renderIcon()}
        width={104}
        height={70}
        alt="heartbeat"
      />
      <p
        style={{ color: heartbeatColor }}
        className="font-[.9rem] accent-green-800"
      >
        {heartbeat}
      </p>
    </SmallCard>
  );
}
