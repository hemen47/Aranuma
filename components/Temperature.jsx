import * as React from "react";
import Image from "next/image";
import iconGreen from "../public/temp-green.png";
import iconOrange from "../public/temp-orange.png";
import iconRed from "../public/temp-red.png";
import SmallCard from "./SmallCard";

export default function Index({
  temperature,
  temperatureColor = "Green",
  showCardSkeleton = true,
}) {
  const renderIcon = () => {
    switch (temperatureColor) {
      case "Green":
        return iconGreen;
      case "Red":
        return iconRed;
      case "Orange":
        return iconOrange;
    }
  };
  return (
    <SmallCard title="دما" showCardSkeleton={showCardSkeleton}>
      <Image
        quality={100}
        src={renderIcon()}
        width={35}
        height={68}
        alt="temperature"
      />
      <p
        style={{ color: temperatureColor }}
        className="font-[.9rem] accent-green-800"
      >
        {temperature}
      </p>
    </SmallCard>
  );
}
