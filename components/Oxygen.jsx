import * as React from "react";
import Image from "next/image";
import iconGreen from "../public/oxygen-green.png";
import iconOrange from "../public/oxygen-orange.png";
import iconRed from "../public/oxygen-red.png";
import SmallCard from "./SmallCard";

export default function Index({ bloodOxygen, bloodOxygenColor = "Green", showCardSkeleton = true }) {
  const renderIcon = () => {
    switch (bloodOxygenColor) {
      case "Green":
        return iconGreen;
      case "Red":
        return iconRed;
      case "Orange":
        return iconOrange;
    }
  };
  return (
    <SmallCard title="اکسیژن" showCardSkeleton={showCardSkeleton}>
      <Image
          quality={100}

          src={renderIcon()}
        width={81}
        height={70}
        alt="Blood Oxygen"
      />
      <p
        style={{ color: bloodOxygenColor }}
        className="font-[.9rem] accent-green-800"
      >
        {bloodOxygen}
      </p>
    </SmallCard>
  );
}
