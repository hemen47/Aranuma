import * as React from "react";
import Image from "next/image";
import icon0 from "../public/battery-0.png";
import icon1 from "../public/battery-1.png";
import icon2 from "../public/battery-2.png";
import icon3 from "../public/battery-3.png";
import SmallCard from "./SmallCard";

export default function Index({ batteryLevel, showCardSkeleton = true }) {
  const renderIcon = () => {
    if (Number(batteryLevel) <= 25) {
      return icon0;
    }
    if (Number(batteryLevel) <= 50 && Number(batteryLevel) > 25) {
      return icon1;
    }
    if (Number(batteryLevel) <= 75 && Number(batteryLevel) > 50) {
      return icon2;
    }
    if (Number(batteryLevel) > 75) {
      return icon3;
    }
  };

  return (
    <SmallCard title="باتری" showCardSkeleton={showCardSkeleton}>
      <Image
          quality={100}
        src={renderIcon()}
        width={70}
        height={70}
        alt="Battery Level"
      />
      <p style={{ color: "#252525" }} className="font-[900] accent-green-800">
        {batteryLevel} %
      </p>
    </SmallCard>
  );
}
