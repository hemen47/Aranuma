import * as React from "react";
import Image from "next/image";
import icon0 from "../public/status0.png";
import icon1 from "../public/status1.png";
import icon2 from "../public/status2.png";
import icon3 from "../public/status3.png";
import icon4 from "../public/status4.png";
import SmallCard from "./SmallCard";
import { useEffect, useState } from "react";

export default function Index({
  status,
  statusColor,
  showCardSkeleton = true,
}) {
  const [text, setText] = useState("Normal");

  const renderIcon = () => {
    switch (status) {
      case 1:
        return icon1;
      case 2:
        return icon2;
      case 3:
        return icon3;
      case 4:
        return icon4;
      default:
        return icon0;
    }
  };

  useEffect(() => {
    switch (status) {
      case 1:
        setText("Level 1");
        break;
      case 2:
        setText("Level 2");
        break;
      case 3:
        setText("Level 3");
        break;
      case 4:
        setText("DANGER");
        break;
      default:
        setText("Normal");
        break;
    }
  }, [status]);

  return (
    <SmallCard title="وضعیت کلی" showCardSkeleton={showCardSkeleton}>
      <Image
        quality={100}
        src={renderIcon()}
        width={35}
        height={71}
        alt="status"
      />
      <p style={{ color: statusColor }} className="font-[900] accent-green-800">
        {text}
      </p>
    </SmallCard>
  );
}
