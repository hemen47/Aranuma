import * as React from "react";
import { Skeleton } from "@mui/material";

export default function SmallCard({ title, children, showCardSkeleton }) {
  return (
    <>
      {showCardSkeleton ? (
        <Skeleton animation="wave" className="w-[10rem] !h-[10rem] m-2 flex justify-center items-center justify-center" variant="rectangular" />
      ) : (
        <div className="m-2 flex justify-center">
          <div className="w-[10rem] h-[10rem] flex flex-col card items-center justify-center">
            <h1 className="text-[.9rem] text-bold mb-2 mr-2 self-start">
              {title}
            </h1>
            {children}
          </div>
        </div>
      )}
    </>
  );
}
